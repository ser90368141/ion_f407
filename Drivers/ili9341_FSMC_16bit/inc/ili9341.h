/*
 * ili9341.h
 *
 *  Created on: 20 ���. 2018 �.
 *      Author: Sergey
 */

#ifndef ILI9341_FSMC_16BIT_INC_ILI9341_H_
#define ILI9341_FSMC_16BIT_INC_ILI9341_H_


#include <stdlib.h>

#include "stm32f4xx_hal.h"
#include "rng.h"
#include "main.h"

#include "DWT_Delay.h"

#include "fonts.h"

#define ADDR_CMD	*(uint16_t*)0x6C000000 // ����� ������ ��� ������
#define ADDR_DATA	*(uint16_t*)0x6C000800 // ����� ������ ��� ������
#define swap(a,b) 	{int16_t t = a; a = b; b = t;}

#define	RESET_ACTIVE	HAL_GPIO_WritePin(RST_TFT_GPIO_Port, RST_TFT_Pin, GPIO_PIN_RESET);
#define	RESET_IDLE		HAL_GPIO_WritePin(RST_TFT_GPIO_Port, RST_TFT_Pin, GPIO_PIN_SET);

#define LIGHT_TFT_ON	HAL_GPIO_WritePin(LIG_TFT_GPIO_Port, LIG_TFT_Pin, GPIO_PIN_SET);
#define LIGHT_TFT_OFF	HAL_GPIO_WritePin(LIG_TFT_GPIO_Port, LIG_TFT_Pin, GPIO_PIN_RESET);

#define BLACK           0x0000      /*   0,   0,   0 */
#define NAVY            0x000F      /*   0,   0, 128 */
#define DGREEN          0x03E0      /*   0, 128,   0 */
#define DCYAN           0x03EF      /*   0, 128, 128 */
#define MAROON          0x7800      /* 128,   0,   0 */
#define PURPLE          0x780F      /* 128,   0, 128 */
#define OLIVE           0x7BE0      /* 128, 128,   0 */
#define LGRAY           0xC618      /* 192, 192, 192 */
#define DGRAY           0x7BEF      /* 128, 128, 128 */
#define BLUE            0x001F      /*   0,   0, 255 */
#define BLUE2			0x051D
#define GREEN           0x07E0      /*   0, 255,   0 */
#define GREEN2		    0xB723
#define GREEN3		    0x8000
#define CYAN            0x07FF      /*   0, 255, 255 */
#define RED             0xF800      /* 255,   0,   0 */
#define MAGENTA         0xF81F      /* 255,   0, 255 */
#define YELLOW          0xFFE0      /* 255, 255,   0 */
#define WHITE           0xFFFF      /* 255, 255, 255 */
#define ORANGE          0xFD20      /* 255, 165,   0 */
#define GREENYELLOW     0xAFE5      /* 173, 255,  47 */
#define BROWN 			0XBC40 //
#define BRRED 			0XFC07 //

//----------------------------------------------------
//Commands
#define ILI9341_RESET				0x0001
#define ILI9341_SLEEP_OUT		  	0x0011
#define ILI9341_GAMMA			    0x0026
#define ILI9341_DISPLAY_OFF			0x0028
#define ILI9341_DISPLAY_ON			0x0029
#define ILI9341_COLUMN_ADDR			0x002A
#define ILI9341_PAGE_ADDR			0x002B
#define ILI9341_GRAM				0x002C
#define ILI9341_TEARING_OFF			0x0034
#define ILI9341_TEARING_ON			0x0035
#define ILI9341_DISPLAY_INVERSION	0x00b4
#define ILI9341_MAC			        0x0036
#define ILI9341_PIXEL_FORMAT    	0x003A
#define ILI9341_WDB			    	0x0051
#define ILI9341_WCD				    0x0053
#define ILI9341_RGB_INTERFACE   	0x00B0
#define ILI9341_FRC					0x00B1
#define ILI9341_BPC					0x00B5
#define ILI9341_DFC				 	0x00B6
#define ILI9341_Entry_Mode_Set		0x00B7
#define ILI9341_POWER1				0x00C0
#define ILI9341_POWER2				0x00C1
#define ILI9341_VCOM1				0x00C5
#define ILI9341_VCOM2				0x00C7
#define ILI9341_POWERA				0x00CB
#define ILI9341_POWERB				0x00CF
#define ILI9341_PGAMMA				0x00E0
#define ILI9341_NGAMMA				0x00E1
#define ILI9341_DTCA				0x00E8
#define ILI9341_DTCB				0x00EA
#define ILI9341_POWER_SEQ			0x00ED
#define ILI9341_3GAMMA_EN			0x00F2
#define ILI9341_INTERFACE			0x00F6
#define ILI9341_PRC				   	0x00F7
#define ILI9341_VERTICAL_SCROLL 	0x0033
#define ILI9341_VSCRSADD 			0x0037
//----------------------------------------------------
//----------------------------------------------------
typedef enum
{
	ROT_NORM_V = 0,
	ROT_NORM_H,
	ROT_BW_V,
	ROT_BW_H,
} TFT9341_Rotation;
//----------------------------------------------------
//----------------------------------------------------

void TFT9341_SetRotation(TFT9341_Rotation r);
uint8_t TFT9341_Init(void);
uint32_t TFT9341_Get_ID(void);
void TFT9341_FillRectangle(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void TFT9341_FillScreen(uint16_t color);
void TFT9341_DrawPixel(int x, int y, uint16_t color);
void TFT9341_DrawLine(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void TFT9341_DrawRect(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2);
void TFT9341_DrawCircle(uint16_t x0, uint16_t y0, int r, uint16_t color);
void TFT9341_DrawChar(uint16_t x, uint16_t y, char ch, const FontDef_t *font,
		uint16_t foreground, uint16_t background);
void TFT9341_DrawStr(uint16_t x, uint16_t y, char* str, const FontDef_t *font,
		uint16_t foreground, uint16_t background);


#endif /* ILI9341_FSMC_16BIT_INC_ILI9341_H_ */
