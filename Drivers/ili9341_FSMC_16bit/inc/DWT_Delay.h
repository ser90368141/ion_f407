#ifndef DWT_DELAY_DWT_DELAY_H_
#define DWT_DELAY_DWT_DELAY_H_

#include "stm32f4xx.h"

#define    DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
#define    DWT_CONTROL   *(volatile unsigned long *)0xE0001000
#define    SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC

static inline void DWT_Delay_us( uint32_t us)
{
	uint32_t t0 = DWT->CYCCNT;
	uint32_t us_count_tic = us * (SystemCoreClock/1000000);
	while ((DWT->CYCCNT - t0) < us_count_tic) ;
}

static inline void DWT_Init(void)
{
	// enable using DWT
	SCB_DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	// Reset counter
	DWT_CYCCNT  = 0;
	// Start counter
	DWT_CONTROL |= DWT_CTRL_CYCCNTENA_Msk;
}

#endif /* DWT_DELAY_DWT_DELAY_H_ */
