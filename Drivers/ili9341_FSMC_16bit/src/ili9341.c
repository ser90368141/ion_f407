/*
 * ili9341.c
 *
 *  Created on: 20 ���. 2018 �.
 *      Author: Sergey
 */

#include "ili9341.h"
//----------------------------------------------------
//----------------------------------------------------
uint16_t X_SIZE = 0;
uint16_t Y_SIZE = 0;

uint32_t lcd_ID = 0;

uint16_t tmp_pos_X = 0;
uint16_t tmp_pos_Y = 0;

void TFT9341_SendCommand(uint16_t cmd)
{
	ADDR_CMD = cmd;
}
void TFT9341_SendData(uint16_t dt)
{
	ADDR_DATA = dt;
}

void TFT9341_reset(void)
{
	RESET_ACTIVE;
	DWT_Delay_us(2000);
	RESET_IDLE;
	TFT9341_SendCommand(0x01); //Software Reset
	for (uint8_t i = 0; i < 3; i++) TFT9341_SendData(0xFF);
}
//----------------------------------------------------
//----------------------------------------------------
uint32_t TFT9341_ReadReg(uint16_t r)
{
	uint32_t id = 0;
	uint8_t x = 0;
	TFT9341_SendCommand(0x0000);
	TFT9341_SendCommand(r);
	DWT_Delay_us(50);
	x = ADDR_DATA;
	id = x;
	id <<= 8;
	DWT_Delay_us(1);
	x = ADDR_DATA;
	id |= x;
	id <<= 8;
	DWT_Delay_us(1);
	x = ADDR_DATA;
	id |= x;
	id <<= 8;
	DWT_Delay_us(1);
	x = ADDR_DATA;
	id |= x;
	if(r == 0xEF)
	{
		id <<= 8;
		DWT_Delay_us(5);
		x = ADDR_DATA;
		id |= x;
	}
	DWT_Delay_us(150);//stabilization time
	return id;
}
uint32_t TFT9341_Get_ID(void)
{
	return TFT9341_ReadReg(0xD3);
}
//----------------------------------------------------
//----------------------------------------------------
void TFT9341_SetRotation(TFT9341_Rotation r)
{
	TFT9341_SendCommand(0x36);
	switch(r)
	{
		case ROT_NORM_V: // normal vert
			TFT9341_SendData(0x48);
			X_SIZE = 240;
			Y_SIZE = 320;
			break;
		case ROT_NORM_H: // normal hor
			TFT9341_SendData(0x28);
			X_SIZE = 320;
			Y_SIZE = 240;
			break;
		case ROT_BW_V: // backward vert
			TFT9341_SendData(0x88);
			X_SIZE = 240;
			Y_SIZE = 320;
			break;
		case ROT_BW_H: // backward hor
			TFT9341_SendData(0xE8);
			X_SIZE = 320;
			Y_SIZE = 240;
			break;
	}
}
void TFT9341_Flood(uint16_t color, uint32_t len) // ������� ������
{
	uint16_t blocks;
	uint8_t i;
	TFT9341_SendCommand (ILI9341_GRAM);
	TFT9341_SendData(color);
	len--;
	blocks = (uint16_t)(len/64);//64 pixels/block
	while(blocks--)
	{
		i = 16;
		do
		{
			TFT9341_SendData(color);
			TFT9341_SendData(color);
			TFT9341_SendData(color);
			TFT9341_SendData(color);
		} while (--i);
	}
	//Fill any remaining pixels(1 to 64)
	for (i = (uint8_t)len&63; i--;)
	{
		TFT9341_SendData(color);
	}
}
void TFT9341_SetAddrWindow(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	TFT9341_SendCommand(ILI9341_COLUMN_ADDR);//Column Addres Set
	TFT9341_SendData(x1 >> 8);
	TFT9341_SendData(x1 & 0xFF);
	TFT9341_SendData(x2 >> 8);
	TFT9341_SendData(x2 & 0xFF);
	TFT9341_SendCommand(ILI9341_PAGE_ADDR);//Page Addres Set
	TFT9341_SendData(y1 >> 8);
	TFT9341_SendData(y1 & 0xFF);
	TFT9341_SendData(y2 >> 8);
	TFT9341_SendData(y2 & 0xFF);
}
//----------------------------------------------------
//----------------------------------------------------
uint8_t TFT9341_Init(void)
{
	DWT_Init();
	LIGHT_TFT_ON;
	TFT9341_reset();
	DWT_Delay_us(1000000);
	TFT9341_SendCommand(ILI9341_RESET);//Software Reset
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_POWERA);//Power Control A
	TFT9341_SendData(0x39);
	TFT9341_SendData(0x2C);
	TFT9341_SendData(0x00);
	TFT9341_SendData(0x34);
	TFT9341_SendData(0x02);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_POWERB);//Power Control B
	TFT9341_SendData(0x00);
	TFT9341_SendData(0xC1);
	TFT9341_SendData(0x30);
	DWT_Delay_us(1);
	TFT9341_SendCommand(0x00E8);//Driver timing control A
	TFT9341_SendData(0x85);
	TFT9341_SendData(0x00);
	TFT9341_SendData(0x78);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_DTCB);//Driver timing control B
	TFT9341_SendData(0x00);
	TFT9341_SendData(0x00);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_POWER_SEQ);//Power on Sequence control
	TFT9341_SendData(0x64);
	TFT9341_SendData(0x03);
	TFT9341_SendData(0x12);
	TFT9341_SendData(0x81);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_PRC);//Pump ratio control
	TFT9341_SendData(0x20);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_POWER1);//Power Control 1
	TFT9341_SendData(0x10);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_POWER2);//Power Control 2
	TFT9341_SendData(0x10);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_VCOM1);//VCOM Control 1
	TFT9341_SendData(0x3E);
	TFT9341_SendData(0x28);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_VCOM2);//VCOM Control 2
	TFT9341_SendData(0x86);
	DWT_Delay_us(1);
	TFT9341_SetRotation(ROT_BW_V);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_PIXEL_FORMAT);//Pixel Format Set
	TFT9341_SendData(0x55);//16bit
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_FRC); // ������� ������
	TFT9341_SendData(0x00);
	TFT9341_SendData(0x18);// ������� ������ 79 ��
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_DFC);//Display Function Control
	TFT9341_SendData(0x08);
	TFT9341_SendData(0x82);
	TFT9341_SendData(0x27);//320 �����
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_3GAMMA_EN);//Enable 3G (���� �� ���� ��� ��� �� �����)
	TFT9341_SendData(0x00);//�� ��������
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_GAMMA);//Gamma set
	TFT9341_SendData(0x01);//Gamma Curve (G2.2) (������ �������� �����)
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_PGAMMA);//Positive Gamma  Correction
	TFT9341_SendData(0x0F);
	TFT9341_SendData(0x31);
	TFT9341_SendData(0x2B);
	TFT9341_SendData(0x0C);
	TFT9341_SendData(0x0E);
	TFT9341_SendData(0x08);
	TFT9341_SendData(0x4E);
	TFT9341_SendData(0xF1);
	TFT9341_SendData(0x37);
	TFT9341_SendData(0x07);
	TFT9341_SendData(0x10);
	TFT9341_SendData(0x03);
	TFT9341_SendData(0x0E);
	TFT9341_SendData(0x09);
	TFT9341_SendData(0x00);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_NGAMMA);//Negative Gamma  Correction
	TFT9341_SendData(0x00);
	TFT9341_SendData(0x0E);
	TFT9341_SendData(0x14);
	TFT9341_SendData(0x03);
	TFT9341_SendData(0x11);
	TFT9341_SendData(0x07);
	TFT9341_SendData(0x31);
	TFT9341_SendData(0xC1);
	TFT9341_SendData(0x48);
	TFT9341_SendData(0x08);
	TFT9341_SendData(0x0F);
	TFT9341_SendData(0x0C);
	TFT9341_SendData(0x31);
	TFT9341_SendData(0x36);
	TFT9341_SendData(0x0F);
	DWT_Delay_us(1);
	TFT9341_SendCommand(ILI9341_SLEEP_OUT);//������ �� ������� �����
	DWT_Delay_us(150000);
	TFT9341_SendCommand(ILI9341_DISPLAY_ON);//��������� �������
	TFT9341_SendData(0x2C);
	DWT_Delay_us(150000);
	return 1;
}
//----------------------------------------------------
void TFT9341_FillScreen(uint16_t color)
{
	TFT9341_SetAddrWindow(0, 0, X_SIZE-1, Y_SIZE-1);
	TFT9341_Flood(color, (long)X_SIZE*(long)Y_SIZE);
}
//----------------------------------------------------
void TFT9341_FillRectangle(uint16_t color, uint16_t x1, uint16_t y1,
		uint16_t x2, uint16_t y2)
{
	TFT9341_SetAddrWindow(x1, y1, x2, y2);
	TFT9341_Flood(color, (uint16_t)(x2 - x1 + 1) * (uint16_t)(y2 - y1 + 1));
}
//----------------------------------------------------
void TFT9341_DrawPixel(int x, int y, uint16_t color)
{
	if((x < 0)||(y < 0)||(x >= X_SIZE)||(y >= Y_SIZE)) return;
    TFT9341_SetAddrWindow(x, y, x, y);
    TFT9341_SendCommand(ILI9341_GRAM);
    TFT9341_SendData(color);
}
//----------------------------------------------------
void TFT9341_DrawLine(uint16_t color, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
	int16_t dx, dy, sx, sy, err, e2;

	dx = (x0 < x1) ? (x1 - x0) : (x0 - x1);
	dy = (y0 < y1) ? (y1 - y0) : (y0 - y1);
	sx = (x0 < x1) ? 1 : -1;
	sy = (y0 < y1) ? 1 : -1;
	err = ((dx > dy) ? dx : -dy) / 2;

	while (1)
	{
		TFT9341_DrawPixel(x0, y0, color);
		if (x0 == x1 && y0 == y1) {
			break;
		}
		e2 = err;
		if (e2 > -dx)
		{
			err -= dy;
			x0 += sx;
		}
		if (e2 < dy)
		{
			err += dx;
			y0 += sy;
		}
	}
}
//----------------------------------------------------
void TFT9341_DrawRect(uint16_t color, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
{
	TFT9341_DrawLine(color,x1,y1,x2,y1);
	TFT9341_DrawLine(color,x2,y1,x2,y2);
	TFT9341_DrawLine(color,x1,y1,x1,y2);
	TFT9341_DrawLine(color,x1,y2,x2,y2);
}
//----------------------------------------------------
void TFT9341_DrawCircle(uint16_t x0, uint16_t y0, int r, uint16_t color)
{
	int f = 1-r;
	int ddF_x = 1;
	int ddF_y = -2*r;
	int x = 0;
	int y = r;
	TFT9341_DrawPixel(x0,y0+r,color);
	TFT9341_DrawPixel(x0,y0-r,color);
	TFT9341_DrawPixel(x0+r,y0,color);
	TFT9341_DrawPixel(x0-r,y0,color);
	while (x<y)
	{
		if (f>=0)
		{
			y--;
			ddF_y+=2;
			f+=ddF_y;
		}
		x++;
		ddF_x+=2;
		f+=ddF_x;
		TFT9341_DrawPixel(x0+x,y0+y,color);
		TFT9341_DrawPixel(x0-x,y0+y,color);
		TFT9341_DrawPixel(x0+x,y0-y,color);
		TFT9341_DrawPixel(x0-x,y0-y,color);
		TFT9341_DrawPixel(x0+y,y0+x,color);
		TFT9341_DrawPixel(x0-y,y0+x,color);
		TFT9341_DrawPixel(x0+y,y0-x,color);
		TFT9341_DrawPixel(x0-y,y0-x,color);
	}
}
//----------------------------------------------------
void TFT9341_DrawChar(uint16_t x, uint16_t y, char ch, const FontDef_t *font,
		uint16_t foreground, uint16_t background)
{
	uint32_t i, b, j;
	/* Set coordinates */
	tmp_pos_X = x;
	tmp_pos_Y = y;
	if ((tmp_pos_X + font->FontWidth) > X_SIZE)
	{
		//If at the end of a line of display, go to new line and set x to 0 position
		if (tmp_pos_Y + font->FontHeight + 1 >= Y_SIZE)
		{
			tmp_pos_Y = tmp_pos_Y + font->FontHeight - Y_SIZE;
		}
		else tmp_pos_Y += font->FontHeight + 1;
		tmp_pos_X = 0;
	}
	for (i = 0; i < font->FontHeight; i++)
	{
		b = font->data[(ch - 32) * font->FontHeight + i];
		for (j = 0; j < font->FontWidth; j++)
		{
			if ((b << j) & 0x8000)
			{
				TFT9341_DrawPixel(tmp_pos_X + j, (tmp_pos_Y + i), foreground);
			} else
			{
				TFT9341_DrawPixel(tmp_pos_X + j, (tmp_pos_Y + i), background);
			}
		}
	}
	tmp_pos_X += font->FontWidth;
}
//----------------------------------------------------
void TFT9341_DrawStr(uint16_t x, uint16_t y, char* str, const FontDef_t *font,
		uint16_t foreground, uint16_t background)
{
	uint16_t startX = x;

	/* Set X and Y coordinates */
	tmp_pos_X = x;
	tmp_pos_Y = y;

	while (*str)
	{
		//New line
		if (*str == '\n')
		{
			if (tmp_pos_Y + font->FontHeight + 1 >= Y_SIZE)
			{
				tmp_pos_Y = tmp_pos_Y + font->FontHeight - Y_SIZE + 1;
			}
			else tmp_pos_Y += font->FontHeight;
			//if after \n is also \r, than go to the left of the screen
			if (*(str + 1) == '\r')
			{
				tmp_pos_X = 0;
				str++;
			}
			else
			{
				tmp_pos_X = startX;
			}
			str++;
			continue;
		}
		else if (*str == '\r')
		{
			str++;
			continue;
		}

		TFT9341_DrawChar(tmp_pos_X, tmp_pos_Y, *str++, font, foreground, background);
	}
}
//----------------------------------------------------
//----------------------------------------------------
void TFT9341_ScrollFrame(uint16_t vsp)
{
	TFT9341_SendCommand(ILI9341_VSCRSADD);
	TFT9341_SendData(vsp >> 8);
	TFT9341_SendData(vsp);
}
//----------------------------------------------------
void TFT9341_Scroll_Setup(uint16_t tfa, uint16_t bfa)
{
	TFT9341_SendCommand(ILI9341_VERTICAL_SCROLL); // Vertical scroll definition
	TFT9341_SendData(tfa >> 8);
	TFT9341_SendData(tfa);
	TFT9341_SendData((320 - tfa - bfa) >> 8);
	TFT9341_SendData(320 - tfa - bfa);
	TFT9341_SendData(bfa >> 8);
	TFT9341_SendData(bfa);
}
void TFT9341_Scroll(FontDef_t *font, uint16_t color)
{
	TFT9341_FillRectangle(color, 0, 0, X_SIZE, font->FontHeight);
	TFT9341_ScrollFrame(font->FontHeight + 1);
}
