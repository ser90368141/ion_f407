#ifndef ILI9341_FSMC_16BIT_INC_RNG_H_
#define ILI9341_FSMC_16BIT_INC_RNG_H_

#include "stm32f4xx_ll_rng.h"

static inline uint32_t TFT_GetRandomData(RNG_TypeDef *RNGx)
{
	while (!LL_RNG_IsActiveFlag_DRDY(RNGx));
	return LL_RNG_ReadRandData32(RNGx);
}

#endif /* ILI9341_FSMC_16BIT_INC_RNG_H_ */
